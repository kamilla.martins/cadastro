let disciplinas = [];
class Disciplina {
  constructor(nomeDaDisciplina, professor) {
    this.nomeDaDisciplina = nomeDaDisciplina;
    this.professor = professor;
  }
}

function cadastrarDisciplinas() {
  let nomeDaDisciplina = byId("disciplina").value;
  let professor = byId("professor").value;
  let disciplina = new Disciplina(nomeDaDisciplina, professor);
  disciplinas.push(disciplina);
  apresentarDisciplinas();
}

function apresentarDisciplinas() {
  let tabela = byId("tabela");
  let listagem  = "<ol>"
  for(let disciplina of disciplinas) {
    listagem += `<li>${disciplina.nomeDaDisciplina} (${disciplina.professor})</li>`
  }
  listagem += "</ol>" 
  tabela.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}